/***************************************
Enter the number of Values : 4

Enter 4 values
1.1 2.2 3.3 4.4

The values entered are	1.1	2.2	3.3	4.4

**************************************
	Sum	 = 	11
	Mean	 = 	2.75
	Variance = 	1.5125
Standard Deviation = 	1.22984
**************************************
============================================================
Enter the number of Values : 5

Enter 5 values
5.345 6.765 7.234 8.675 9.765

The values entered are	5.345	6.765	7.234	8.675	9.765

**************************************
	Sum	 = 	37.784
	Mean	 = 	7.5568
	Variance = 	2.34995
Standard Deviation = 	1.53295
**************************************
***************************************/
