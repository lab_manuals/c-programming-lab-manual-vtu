/***************************************************************************
*File			: A11BinarytoDecimal.c
*Description	: Program to compute Mean, Variance and Standard Deviation 
*					using pointer to an array
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>

unsigned long fnBin2Dec(unsigned long);

/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main(void)
{
	unsigned long iBinVal, iDecVal;
	
	printf("\nEnter the binary value : ");
	scanf("%lu", &iBinVal);
	
	iDecVal = fnBin2Dec(iBinVal);
	
	printf("\nDecimal equivalent of %lu is %lu\n", iBinVal, iDecVal);
	return 0;
}

/***************************************************************************
*Function			: fnBin2Dec
*Description		: Function to convert a binary value to its 
*						equivalent decimal value
*Input parameters	: unsigned long iVal	- binary value to be converted
*RETURNS			: equivalent decimal value
***************************************************************************/

unsigned long fnBin2Dec(unsigned long iVal)
{
	unsigned long  iNewVal, iLastDigit;
	if(1 == iVal || 0 == iVal)
		return iVal;
	else
	{
		iNewVal = iVal/10;
		iLastDigit = iVal%10;
		return fnBin2Dec(iNewVal)* 2 + fnBin2Dec(iLastDigit);	
	}
}
