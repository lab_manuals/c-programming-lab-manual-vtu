/***************************************************************************
*File			: A04BinarySearch.c
*Description	: Program to perform a binary search on Array of names
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/

int main(void)
{
	int iNum,i,iMid,iLow,iHigh,iFound;

	char stNames[100][40], srchKey[40];
	printf("\nEnter the number of names\n");
	scanf("%d",&iNum);
	getchar();

	printf("\nEnter the elements in ascending order\n");
	for(i=0;i<iNum;i++)
	{
		gets(stNames[i]);
	}


	printf("\nEnter the name to be searched\n");
	gets(srchKey);

	iFound = 0;
	iLow = 0;
	iHigh = iNum-1;
	while(iLow <= iHigh)
	{
		iMid = (iLow+iHigh)/2;
		if(strcmp(srchKey, stNames[iMid]) == 0)
		{
			iFound = 1;
			break;
		}
		else if(strcmp(srchKey, stNames[iMid]) < 0)
		{
			iHigh = iMid-1;
		}
		else
		{
			iLow = iMid+1;
		}
	}

	if(iFound)
		printf("\nThe name %s is found at position %d\n",srchKey,iMid+1);
	else
		printf("\nThe name %s is not found\n",srchKey);

	return 0;
}

