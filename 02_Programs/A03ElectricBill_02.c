#include <stdio.h>
#include <stdlib.h>

#define SLAB1 0.8
#define SLAB2 0.9
#define BILL_MIN 100
#define SURCHARGE_RATE 0.15
int main()
{
    char stName[40];
    int iNumUnits;
    float fAmount;

    printf("\nEnter Customer name:");
    gets(stName);
    printf("\nEnter number of units consumed: ");
    scanf("%d", &iNumUnits);

    printf("ELECTRIC BILL\n");
    printf("%-25s%s\n", "Name : ",stName);
    printf("%-25s%d units\n", "Units Consumed : ", iNumUnits);

    if(iNumUnits < 200)
    {
        fAmount = BILL_MIN + iNumUnits * SLAB1;
        printf("%-24s= %7.2f Rs\n","Bill Minimum ",100.0);
        printf("\t%3d *  80 paise = %7.2f\n", iNumUnits,iNumUnits * SLAB1);
    }
    else if(iNumUnits < 300)
    {
        fAmount = BILL_MIN + (200 * SLAB1) + (iNumUnits-200) * SLAB2;
        printf("%-24s= %7.2f Rs\n","Bill Minimum ",100.0);
        printf("\t%3d *  80 paise = %7.2f\n", 200, 200 * SLAB1);
        printf("\t%3d *  90 paise = %7.2f\n", (iNumUnits-200),(iNumUnits-200) * SLAB2);
    }
    else
    {
        fAmount = BILL_MIN + (200 * SLAB1) + (100 * SLAB2) + (iNumUnits-300);
        printf("%-24s= %7.2f Rs\n","Bill Minimum ",100.0);
        printf("\t%3d *  80 paise = %7.2f Rs\n", 200, 200 * SLAB1);
        printf("\t%3d *  90 paise = %7.2f Rs\n", 100, 100 * SLAB2);
        printf("\t%3d * 100 paise = %7.2f Rs\n", (iNumUnits-300),(float)(iNumUnits-300));
    }
    if(fAmount > 400) //Add surcharge at the rate of 15% if any
    {
        fAmount = 1.15 * fAmount;
        printf("%-23s = %7.2f Rs\n","Surcharge Amount", fAmount *0.15);
    }

    printf("%-23s = %7.2f Rs\n", "Total Bill Charges ", fAmount);
    return 0;
}

