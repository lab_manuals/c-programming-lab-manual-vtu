/****************************

Enter the number of names
5

Enter the elements in ascending order
Adi
Bob
Ram
Sam
Tom

Enter the name to be searched
Tom

The name Tom is found at position 5
===================================

Enter the number of names
4

Enter the elements in ascending order
Doug
Fred
John
Stan

Enter the name to be searched
Fred

The name Fred is found at position 2
===================================

Enter the number of names
3

Enter the elements in ascending order
Ankit
Deepu     
Shiva

Enter the name to be searched
Guru

The name Guru is not found

***************************************/

