/***************************************************************************
*File			: B13StudentStructure.c
*Description	: Program to implement structure and compute average marks
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 18.04
*Date			: 16 August 2018
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#define STRSIZE 30

typedef struct
{
	char cName[STRSIZE];
	char cUSN[11];
	int iMarks;
}STUDENT_TYPE;

/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/

int main(void)
{
	STUDENT_TYPE students[100];
	int iNum, i;
	double dAvg = 0.0;
	
	printf("\nEnter the number of students : ");
	scanf("%d", &iNum);
	
	printf("\nEnter the Student details\n");
	for(i=0;i<iNum;i++)
	{
		printf("\n###############################");
		printf("\nName : "); 	scanf("%s", students[i].cName);
		printf("\nUSN : "); 	scanf("%s", students[i].cUSN);
		printf("\nMarks : "); 	scanf("%d", &students[i].iMarks);
		dAvg += students[i].iMarks;
	}
	
	dAvg /= iNum;
	
	printf("\nThe average marks for the class is : %g\n", dAvg);
	
	for(i=0;i<iNum;i++)
	{
		printf("\n###############################");		
		printf("\nName\t: %s", students[i].cName);
		printf("\nUSN\t: %s", students[i].cUSN);
		printf("\nMarks\t: %d", students[i].iMarks);
		if(students[i].iMarks < dAvg)
			printf("\nThe student has scored below average\n");
		else
			printf("\nThe student has scored above average\n");
	}
		
	return 0;
}
