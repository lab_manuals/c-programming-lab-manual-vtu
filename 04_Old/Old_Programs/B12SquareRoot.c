/***************************************************************************
*File			: B12SquareRoot.c
*Description	: Program to find the square root of a given number
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 18.04
*Date			: 16 August 2018
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include <math.h>
#include <float.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main()
{
    float fVal, fNextGuess, fLastGuess = 1.0f, fDiff ;
    printf("\nEnter a value whose square root has to be calculated\n");
    scanf("%f", &fVal);
    do
    {
        fNextGuess = 0.5 * (fLastGuess + (fVal/fLastGuess));
        fDiff = fabs(fNextGuess - fLastGuess);
        fLastGuess = fNextGuess;
    }while (fDiff > FLT_EPSILON);
/*    }while (fDiff > 0.0001);*/

    printf("\nSquare root of %g = %g\n", fVal, fNextGuess);
    //Comparison with built in function
    //printf("\nSquare root of %g = %g\n", fVal, sqrt(fVal));	
    return 0;
}
