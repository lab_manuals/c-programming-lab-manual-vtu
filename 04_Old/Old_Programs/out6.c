/***************************************
******************************************************
*	PROGRAM TO IMPLEMENT TEST OF PRIMALITY	     *
******************************************************
Enter the value to be checked
45
The entered value 45 is not a prime number

******************************************************
*	PROGRAM TO IMPLEMENT TEST OF PRIMALITY	     *
******************************************************
Enter the value to be checked
41
The entered value 41 is a prime number

******************************************************
*	PROGRAM TO IMPLEMENT TEST OF PRIMALITY	     *
******************************************************
Enter the value to be checked
17
The entered value 17 is a prime number

******************************************************
*	PROGRAM TO IMPLEMENT TEST OF PRIMALITY	     *
******************************************************
Enter the value to be checked
15
The entered value 15 is not a prime number

***************************************/

