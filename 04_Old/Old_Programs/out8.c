/***************************************
Enter the Angle : 60
Enter the Number of terms : 3

Input Angle = 60
No of terms = 3

Calculated value is :
Sin(60) = 0.866295

Built In function value is :
Sin(60) = 0.866025

=========================================
Enter the Angle : 60
Enter the Number of terms : 9

Input Angle = 60
No of terms = 9

Calculated value is :
Sin(60) = 0.866025

Built In function value is :
Sin(60) = 0.866025

=========================================
Enter the Angle : 30
Enter the Number of terms : 2

Input Angle = 30
No of terms = 2

Calculated value is :
Sin(30) = 0.499674

Built In function value is :
Sin(30) = 0.5

=========================================
Enter the Angle : 30
Enter the Number of terms : 8

Input Angle = 30
No of terms = 8

Calculated value is :
Sin(30) = 0.5

Built In function value is :
Sin(30) = 0.5
***************************************/

