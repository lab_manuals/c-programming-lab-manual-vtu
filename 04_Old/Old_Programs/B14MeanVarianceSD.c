/***************************************************************************
*File			: B14MeanVarianceSD.c
*Description	: Program to compute Mean, Variance and Standard Deviation 
					using pointer to an array
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 18.04
*Date			: 16 August 2018
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main(void)
{
    int i,iNum;
    float fMean = 0.0f, fVariance = 0.0f, fSd = 0.0f,faArray[100],fSum=0.0f;
    float *fptr;
    
    fptr = faArray;
    printf("\nEnter the number of Values : ");
    scanf("%d",&iNum);
    printf("\nEnter %d values\n", iNum);
    for(i=0; i<iNum; i++)
    {
        scanf("%f",fptr+i);
        fSum += *(fptr+i);		//fSum += fptr[i]; this is also valid
    }
    fMean = fSum/iNum;

    for(i=0; i<iNum; i++)
    {
        fVariance += (fptr[i] - fMean)*(fptr[i] - fMean);
		//fVariance += (*(fptr+i) - fMean)*(*(fptr+i) - fMean);
    }
    fVariance /= iNum;
    fSd = sqrt(fVariance);
    printf("\nThe values entered are");
    for(i=0; i<iNum; i++)
    {
        printf("\n\t%g",fptr[i]);        //printf("\n\t%f",*(fptr+i));
    }

    printf("\n**************************************\n");
    printf("\n\tMean = \t%g\n\tVariance = \t%g\n\tStandard Deviation = \t%g\n",fMean,fVariance,fSd);
    printf("\n**************************************\n");
    return 0;
}
